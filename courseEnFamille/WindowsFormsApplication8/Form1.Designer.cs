﻿namespace WindowsFormsApplication8
{
    partial class For
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtAjouter = new System.Windows.Forms.TextBox();
            this.lblCourse = new System.Windows.Forms.Label();
            this.btnAjouter = new System.Windows.Forms.Button();
            this.btnFait = new System.Windows.Forms.Button();
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.btnTerminer = new System.Windows.Forms.Button();
            this.btnEffacer = new System.Windows.Forms.Button();
            this.lstCourseAFaire = new System.Windows.Forms.ListBox();
            this.lstCourseFaite = new System.Windows.Forms.ListBox();
            this.lblNbreAFaire = new System.Windows.Forms.Label();
            this.lblNbreFaite = new System.Windows.Forms.Label();
            this.lblFait = new System.Windows.Forms.Label();
            this.lblAFaire = new System.Windows.Forms.Label();
            this.ChkSaumon = new System.Windows.Forms.CheckBox();
            this.ChkThon = new System.Windows.Forms.CheckBox();
            this.ChkCrevette = new System.Windows.Forms.CheckBox();
            this.RbtnNature = new System.Windows.Forms.RadioButton();
            this.RbtnRiz = new System.Windows.Forms.RadioButton();
            this.RbtnAlgue = new System.Windows.Forms.RadioButton();
            this.lblPrixSaumon = new System.Windows.Forms.Label();
            this.lblPrixThon = new System.Windows.Forms.Label();
            this.LblPrixCrevette = new System.Windows.Forms.Label();
            this.PrixNature = new System.Windows.Forms.Label();
            this.PrixRiz = new System.Windows.Forms.Label();
            this.PrixAlgue = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPrixSupplément = new System.Windows.Forms.Label();
            this.BtnSupplément = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtAjouter
            // 
            this.txtAjouter.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAjouter.Location = new System.Drawing.Point(32, 83);
            this.txtAjouter.Name = "txtAjouter";
            this.txtAjouter.Size = new System.Drawing.Size(100, 21);
            this.txtAjouter.TabIndex = 0;
            // 
            // lblCourse
            // 
            this.lblCourse.AutoSize = true;
            this.lblCourse.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCourse.Location = new System.Drawing.Point(157, 28);
            this.lblCourse.Name = "lblCourse";
            this.lblCourse.Size = new System.Drawing.Size(222, 27);
            this.lblCourse.TabIndex = 1;
            this.lblCourse.Text = "Les Courses de Famille";
            // 
            // btnAjouter
            // 
            this.btnAjouter.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAjouter.Location = new System.Drawing.Point(224, 178);
            this.btnAjouter.Name = "btnAjouter";
            this.btnAjouter.Size = new System.Drawing.Size(75, 23);
            this.btnAjouter.TabIndex = 2;
            this.btnAjouter.Text = "Ajouter";
            this.btnAjouter.UseVisualStyleBackColor = true;
            this.btnAjouter.Click += new System.EventHandler(this.btnAjouter_Click);
            // 
            // btnFait
            // 
            this.btnFait.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFait.Location = new System.Drawing.Point(224, 226);
            this.btnFait.Name = "btnFait";
            this.btnFait.Size = new System.Drawing.Size(75, 23);
            this.btnFait.TabIndex = 3;
            this.btnFait.Text = "Fait";
            this.btnFait.UseVisualStyleBackColor = true;
            this.btnFait.Click += new System.EventHandler(this.btnFait_Click);
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnnuler.Location = new System.Drawing.Point(224, 271);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(75, 23);
            this.btnAnnuler.TabIndex = 4;
            this.btnAnnuler.Text = "Annuler";
            this.btnAnnuler.UseVisualStyleBackColor = true;
            this.btnAnnuler.Click += new System.EventHandler(this.btnAnnuler_Click);
            // 
            // btnTerminer
            // 
            this.btnTerminer.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTerminer.Location = new System.Drawing.Point(224, 377);
            this.btnTerminer.Name = "btnTerminer";
            this.btnTerminer.Size = new System.Drawing.Size(75, 23);
            this.btnTerminer.TabIndex = 5;
            this.btnTerminer.Text = "Terminer";
            this.btnTerminer.UseVisualStyleBackColor = true;
            // 
            // btnEffacer
            // 
            this.btnEffacer.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEffacer.Location = new System.Drawing.Point(224, 323);
            this.btnEffacer.Name = "btnEffacer";
            this.btnEffacer.Size = new System.Drawing.Size(75, 23);
            this.btnEffacer.TabIndex = 6;
            this.btnEffacer.Text = "Effacer";
            this.btnEffacer.UseVisualStyleBackColor = true;
            this.btnEffacer.Click += new System.EventHandler(this.btnEffacer_Click);
            // 
            // lstCourseAFaire
            // 
            this.lstCourseAFaire.Font = new System.Drawing.Font("Berlin Sans FB", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstCourseAFaire.FormattingEnabled = true;
            this.lstCourseAFaire.ItemHeight = 17;
            this.lstCourseAFaire.Location = new System.Drawing.Point(23, 178);
            this.lstCourseAFaire.Name = "lstCourseAFaire";
            this.lstCourseAFaire.Size = new System.Drawing.Size(174, 259);
            this.lstCourseAFaire.TabIndex = 7;
            // 
            // lstCourseFaite
            // 
            this.lstCourseFaite.Font = new System.Drawing.Font("Berlin Sans FB", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstCourseFaite.FormattingEnabled = true;
            this.lstCourseFaite.ItemHeight = 17;
            this.lstCourseFaite.Location = new System.Drawing.Point(329, 178);
            this.lstCourseFaite.Name = "lstCourseFaite";
            this.lstCourseFaite.Size = new System.Drawing.Size(174, 259);
            this.lstCourseFaite.TabIndex = 8;
            // 
            // lblNbreAFaire
            // 
            this.lblNbreAFaire.AutoSize = true;
            this.lblNbreAFaire.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNbreAFaire.Location = new System.Drawing.Point(97, 460);
            this.lblNbreAFaire.Name = "lblNbreAFaire";
            this.lblNbreAFaire.Size = new System.Drawing.Size(11, 16);
            this.lblNbreAFaire.TabIndex = 9;
            this.lblNbreAFaire.Text = " ";
            // 
            // lblNbreFaite
            // 
            this.lblNbreFaite.AutoSize = true;
            this.lblNbreFaite.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNbreFaite.Location = new System.Drawing.Point(382, 460);
            this.lblNbreFaite.Name = "lblNbreFaite";
            this.lblNbreFaite.Size = new System.Drawing.Size(11, 16);
            this.lblNbreFaite.TabIndex = 10;
            this.lblNbreFaite.Text = " ";
            // 
            // lblFait
            // 
            this.lblFait.AutoSize = true;
            this.lblFait.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFait.Location = new System.Drawing.Point(397, 148);
            this.lblFait.Name = "lblFait";
            this.lblFait.Size = new System.Drawing.Size(75, 14);
            this.lblFait.TabIndex = 11;
            this.lblFait.Text = "Course faite";
            this.lblFait.Click += new System.EventHandler(this.label4_Click);
            // 
            // lblAFaire
            // 
            this.lblAFaire.AutoSize = true;
            this.lblAFaire.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAFaire.Location = new System.Drawing.Point(51, 148);
            this.lblAFaire.Name = "lblAFaire";
            this.lblAFaire.Size = new System.Drawing.Size(86, 14);
            this.lblAFaire.TabIndex = 12;
            this.lblAFaire.Text = "Course à faire";
            // 
            // ChkSaumon
            // 
            this.ChkSaumon.AutoSize = true;
            this.ChkSaumon.Location = new System.Drawing.Point(57, 479);
            this.ChkSaumon.Name = "ChkSaumon";
            this.ChkSaumon.Size = new System.Drawing.Size(65, 17);
            this.ChkSaumon.TabIndex = 13;
            this.ChkSaumon.Text = "Saumon";
            this.ChkSaumon.UseVisualStyleBackColor = true;
            // 
            // ChkThon
            // 
            this.ChkThon.AutoSize = true;
            this.ChkThon.Location = new System.Drawing.Point(57, 513);
            this.ChkThon.Name = "ChkThon";
            this.ChkThon.Size = new System.Drawing.Size(51, 17);
            this.ChkThon.TabIndex = 14;
            this.ChkThon.Text = "Thon";
            this.ChkThon.UseVisualStyleBackColor = true;
            // 
            // ChkCrevette
            // 
            this.ChkCrevette.AutoSize = true;
            this.ChkCrevette.Location = new System.Drawing.Point(57, 549);
            this.ChkCrevette.Name = "ChkCrevette";
            this.ChkCrevette.Size = new System.Drawing.Size(66, 17);
            this.ChkCrevette.TabIndex = 15;
            this.ChkCrevette.Text = "Crevette";
            this.ChkCrevette.UseVisualStyleBackColor = true;
            // 
            // RbtnNature
            // 
            this.RbtnNature.AutoSize = true;
            this.RbtnNature.Checked = true;
            this.RbtnNature.Location = new System.Drawing.Point(329, 479);
            this.RbtnNature.Name = "RbtnNature";
            this.RbtnNature.Size = new System.Drawing.Size(57, 17);
            this.RbtnNature.TabIndex = 16;
            this.RbtnNature.TabStop = true;
            this.RbtnNature.Text = "Nature";
            this.RbtnNature.UseVisualStyleBackColor = true;
            // 
            // RbtnRiz
            // 
            this.RbtnRiz.AutoSize = true;
            this.RbtnRiz.Location = new System.Drawing.Point(329, 513);
            this.RbtnRiz.Name = "RbtnRiz";
            this.RbtnRiz.Size = new System.Drawing.Size(68, 17);
            this.RbtnRiz.TabIndex = 17;
            this.RbtnRiz.Text = "Avec Riz";
            this.RbtnRiz.UseVisualStyleBackColor = true;
            // 
            // RbtnAlgue
            // 
            this.RbtnAlgue.AutoSize = true;
            this.RbtnAlgue.Location = new System.Drawing.Point(329, 549);
            this.RbtnAlgue.Name = "RbtnAlgue";
            this.RbtnAlgue.Size = new System.Drawing.Size(79, 17);
            this.RbtnAlgue.TabIndex = 18;
            this.RbtnAlgue.Text = "avec Algue";
            this.RbtnAlgue.UseVisualStyleBackColor = true;
            // 
            // lblPrixSaumon
            // 
            this.lblPrixSaumon.AutoSize = true;
            this.lblPrixSaumon.Location = new System.Drawing.Point(147, 479);
            this.lblPrixSaumon.Name = "lblPrixSaumon";
            this.lblPrixSaumon.Size = new System.Drawing.Size(19, 13);
            this.lblPrixSaumon.TabIndex = 19;
            this.lblPrixSaumon.Text = "20";
            // 
            // lblPrixThon
            // 
            this.lblPrixThon.AutoSize = true;
            this.lblPrixThon.Location = new System.Drawing.Point(147, 517);
            this.lblPrixThon.Name = "lblPrixThon";
            this.lblPrixThon.Size = new System.Drawing.Size(19, 13);
            this.lblPrixThon.TabIndex = 20;
            this.lblPrixThon.Text = "21";
            // 
            // LblPrixCrevette
            // 
            this.LblPrixCrevette.AutoSize = true;
            this.LblPrixCrevette.Location = new System.Drawing.Point(147, 553);
            this.LblPrixCrevette.Name = "LblPrixCrevette";
            this.LblPrixCrevette.Size = new System.Drawing.Size(19, 13);
            this.LblPrixCrevette.TabIndex = 21;
            this.LblPrixCrevette.Text = "15";
            // 
            // PrixNature
            // 
            this.PrixNature.AutoSize = true;
            this.PrixNature.Location = new System.Drawing.Point(453, 481);
            this.PrixNature.Name = "PrixNature";
            this.PrixNature.Size = new System.Drawing.Size(13, 13);
            this.PrixNature.TabIndex = 22;
            this.PrixNature.Text = "0";
            // 
            // PrixRiz
            // 
            this.PrixRiz.AutoSize = true;
            this.PrixRiz.Location = new System.Drawing.Point(453, 517);
            this.PrixRiz.Name = "PrixRiz";
            this.PrixRiz.Size = new System.Drawing.Size(13, 13);
            this.PrixRiz.TabIndex = 23;
            this.PrixRiz.Text = "5";
            // 
            // PrixAlgue
            // 
            this.PrixAlgue.AutoSize = true;
            this.PrixAlgue.Location = new System.Drawing.Point(447, 553);
            this.PrixAlgue.Name = "PrixAlgue";
            this.PrixAlgue.Size = new System.Drawing.Size(19, 13);
            this.PrixAlgue.TabIndex = 24;
            this.PrixAlgue.Text = "15";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(185, 460);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "Supplément Sushi du mardi soir";
            // 
            // lblPrixSupplément
            // 
            this.lblPrixSupplément.AutoSize = true;
            this.lblPrixSupplément.Location = new System.Drawing.Point(185, 609);
            this.lblPrixSupplément.Name = "lblPrixSupplément";
            this.lblPrixSupplément.Size = new System.Drawing.Size(16, 13);
            this.lblPrixSupplément.TabIndex = 26;
            this.lblPrixSupplément.Text = "...";
            // 
            // BtnSupplément
            // 
            this.BtnSupplément.Location = new System.Drawing.Point(224, 582);
            this.BtnSupplément.Name = "BtnSupplément";
            this.BtnSupplément.Size = new System.Drawing.Size(75, 23);
            this.BtnSupplément.TabIndex = 27;
            this.BtnSupplément.Text = "Calculer Supplément";
            this.BtnSupplément.UseVisualStyleBackColor = true;
            this.BtnSupplément.Click += new System.EventHandler(this.BtnSupplément_Click);
            // 
            // For
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(515, 657);
            this.Controls.Add(this.BtnSupplément);
            this.Controls.Add(this.lblPrixSupplément);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PrixAlgue);
            this.Controls.Add(this.PrixRiz);
            this.Controls.Add(this.PrixNature);
            this.Controls.Add(this.LblPrixCrevette);
            this.Controls.Add(this.lblPrixThon);
            this.Controls.Add(this.lblPrixSaumon);
            this.Controls.Add(this.RbtnAlgue);
            this.Controls.Add(this.RbtnRiz);
            this.Controls.Add(this.RbtnNature);
            this.Controls.Add(this.ChkCrevette);
            this.Controls.Add(this.ChkThon);
            this.Controls.Add(this.ChkSaumon);
            this.Controls.Add(this.lblAFaire);
            this.Controls.Add(this.lblFait);
            this.Controls.Add(this.lblNbreFaite);
            this.Controls.Add(this.lblNbreAFaire);
            this.Controls.Add(this.lstCourseFaite);
            this.Controls.Add(this.lstCourseAFaire);
            this.Controls.Add(this.btnEffacer);
            this.Controls.Add(this.btnTerminer);
            this.Controls.Add(this.btnAnnuler);
            this.Controls.Add(this.btnFait);
            this.Controls.Add(this.btnAjouter);
            this.Controls.Add(this.lblCourse);
            this.Controls.Add(this.txtAjouter);
            this.Name = "For";
            this.Text = "FormLesCourseDeFamille";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblCourse;
        private System.Windows.Forms.Button btnAjouter;
        private System.Windows.Forms.Button btnFait;
        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.Button btnTerminer;
        private System.Windows.Forms.Button btnEffacer;
        private System.Windows.Forms.ListBox lstCourseAFaire;
        private System.Windows.Forms.ListBox lstCourseFaite;
        private System.Windows.Forms.Label lblNbreAFaire;
        private System.Windows.Forms.Label lblNbreFaite;
        private System.Windows.Forms.Label lblFait;
        private System.Windows.Forms.Label lblAFaire;
        private System.Windows.Forms.TextBox txtAjouter;
        private System.Windows.Forms.CheckBox ChkSaumon;
        private System.Windows.Forms.CheckBox ChkThon;
        private System.Windows.Forms.CheckBox ChkCrevette;
        private System.Windows.Forms.RadioButton RbtnNature;
        private System.Windows.Forms.RadioButton RbtnRiz;
        private System.Windows.Forms.RadioButton RbtnAlgue;
        private System.Windows.Forms.Label lblPrixSaumon;
        private System.Windows.Forms.Label lblPrixThon;
        private System.Windows.Forms.Label LblPrixCrevette;
        private System.Windows.Forms.Label PrixNature;
        private System.Windows.Forms.Label PrixRiz;
        private System.Windows.Forms.Label PrixAlgue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblPrixSupplément;
        private System.Windows.Forms.Button BtnSupplément;
    }
}

