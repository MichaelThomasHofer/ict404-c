﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication8
{
    public partial class For : Form
    {
        public For()
        {
            InitializeComponent();
        }
        int NbreAFaire = 0, NbreFait = 0;
        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void btnAjouter_Click(object sender, EventArgs e)
        {
            if (txtAjouter.Text == "")
            {
                MessageBox.Show("Erreur");
            }
            else { lstCourseAFaire.Items.Add(txtAjouter.Text);
                txtAjouter.Text = "";


                NbreFait = lstCourseFaite.Items.Count;
                lblNbreFaite.Text = NbreFait + " Courses ont été faites";

                NbreAFaire = lstCourseAFaire.Items.Count;
                lblNbreAFaire.Text = "il reste " + NbreAFaire + " course à faire";
            }

        }

        private void btnFait_Click(object sender, EventArgs e)
        {
            if (lstCourseAFaire.SelectedItems.Count == 0)
            {
                MessageBox.Show("Erreur");
            }
            else { lstCourseFaite.Items.Add(lstCourseAFaire.SelectedItem);
                lstCourseAFaire.Items.Remove(lstCourseAFaire.SelectedItem);
            }
            NbreFait = lstCourseFaite.Items.Count;
            lblNbreFaite.Text = NbreFait + " Courses ont été faites";

            NbreAFaire = lstCourseAFaire.Items.Count;
            lblNbreAFaire.Text = "il reste " + NbreAFaire + " course à faire";
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            if (lstCourseFaite.SelectedItems.Count == 0)
            {
                MessageBox.Show("erreur");
            }
            else
            {
                lstCourseAFaire.Items.Add(lstCourseFaite.SelectedItem);
                lstCourseFaite.Items.Remove(lstCourseFaite.SelectedItem);
            }
            NbreFait = lstCourseFaite.Items.Count;
            lblNbreFaite.Text = NbreFait + " Courses ont été faites";

            NbreAFaire = lstCourseAFaire.Items.Count;
            lblNbreAFaire.Text = "il reste " + NbreAFaire + " course à faire";
        }

        private void BtnSupplément_Click(object sender, EventArgs e)
        {
            int Resultatp = 0, resultatS = 0, Resultat = 0;

            if (ChkCrevette.Checked == true)
            {
                Resultatp = Resultatp + 15;
            }
            if (ChkSaumon.Checked == true)
            {
                Resultatp = Resultatp + 20;
            }
            if (ChkThon.Checked == true)
            {
                Resultatp = Resultatp + 15;
            }
            if (RbtnAlgue.Checked == true)
            {
                resultatS = resultatS + 15;
            }
            if (RbtnRiz.Checked == true)
            {
                resultatS = resultatS + 5;
            }
            Resultat = resultatS + Resultatp;
            lblPrixSupplément.Text = "Le supplément du mardi soir coutera " + Resultat + " chf";

        }

        private void btnEffacer_Click(object sender, EventArgs e)
        {

            if (lstCourseFaite.SelectedItems.Count == 0)
            {
                if (lstCourseAFaire.SelectedItems.Count == 0)
                {
                    MessageBox.Show("Erreuycfbyyr");
                }
                else
                {
       
                    lstCourseAFaire.Items.Remove(lstCourseAFaire.SelectedItem);
                }
            }
            else
            {
                lstCourseFaite.Items.Remove(lstCourseFaite.SelectedItem);
                if (lstCourseAFaire.SelectedItems.Count == 0)
                {
                    MessageBox.Show("Erreuycfbyyr");
                }
                else
                {

                    lstCourseAFaire.Items.Remove(lstCourseAFaire.SelectedItem);
                }

            }
            NbreFait = lstCourseFaite.Items.Count;
            lblNbreFaite.Text = NbreFait + " Courses ont été faites";

            NbreAFaire = lstCourseAFaire.Items.Count;
            lblNbreAFaire.Text = "il reste "+NbreAFaire + " course à faire";
        }
    }
}
